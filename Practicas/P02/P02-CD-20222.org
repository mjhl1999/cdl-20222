#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [spanish,12pt,letterpaper]
#+LATEX_HEADER: \input{../tex_res/header.tex}
#+OPTIONS: toc:nil author:nil

#+LATEX_HEADER: \title{Computación Distribuida \\ Práctica 02}
#+LATEX_HEADER: \author{Profesor: Víctor Zamora Gutiérrez\\ Ayudante: José Ricardo Desales Santos\\ Ayudante: Diego Estrada Mejía\\ Laboratorio: Emiliano Galeana Araujo\\ Laboratorio: Miren Jessamyn Hernández Leyva}
#+LATEX_HEADER: \affil{Facultad de ciencias, UNAM}
#+DATE: Fecha de entrega: 28 Febrero 2022

\maketitle
* Parte 1

Para  cada  una  de  las siguientes  funciones  tendrás  que  importar
únicamente  en  la  definición  de  la  función  las  bibliotecas  que
necesites,  además, si  usas alguna  función de  una biblioteca,  solo
importa dicha función.

1. Función que dado un número $n$  y una cadena, regresa una lista con
   $n$ veces la cadena.
2. Función que  dada una lista, un  índice $i$ y un  valor, regresa la
   lista con el valor insertado en el índice $i$ de la lista.
3. Función que dada una lista y  un índice $i$ regresa la lista sin el
   elemento en la posición $i$.
4. Función que regresa el último elemento de una lista.
5. Función que  dada  una lista  de listas  encapsula  en tuplas  los
  elementos correspondientes de cada lista, es decir, encapsula todos
  los elementos con índice 0, todos los elementos con índice 1, etc.
6. Función  que dado  un =map= y  una llave, regresa  el =map=  sin la
   entrada con la llave.
7. Función que dado un =map= regresa su conversión a una lista.
8. Función que calcula la distancia entre dos puntos.
9. Función que inserta un elemento en una tupla.
10. Función que pasa de una tupla a una lista

* Parte 2

Utilizando  *unicamente*  las  funciones  definidas  en  el  ejercicio
anterior realiza lo siguiente:

1. Crea un atributo que sea una lista del 1 al 10
2. Función que dada una cadena, un elemento, un número $n$ y un índice
   $i < n$, crea una lista con  la cadena repetida "n" veces, y a esta
   lista, le agrega el elemento en el índice dado.

   El siguiente código ejemplifica  el comportamiento, no es necesario
   que el módulo ni la función se llamen igual:

   #+BEGIN_SRC elixir
   Consultas.ejercicio2("abc", "ijk", 5, 3)
   # Regresa: ["abc", "abc", "abc", "ijk", "abc", "abc"]
   #+END_SRC
3. Crea un módulo, las siguientes  funciones deberán ir dentro de este
   nuevo módulo.
   1.  Función que  dado  un map  y  un índice,  elimina  de la  lista
      generada por el map, el elemento en el índice dado.
   2. Función  que dada  una tupla y  un valor, agrega  el valor  a la
      tupla,  pasa la  tupla a  lista y  finalmente regresa  el último
      elemento de esta lista.


* Hints
+ La fórmula  para la distancia ocupa  sacar la raíz y  elevar algo al
  cuadrado.
+ No  es hint, pero es  aclaración, deberán entregar dos  archivos uno
  para la parte 1 y otro para la parte 2
+ [[https://hexdocs.pm/elixir/List.html][bibliotecas]]

* Lineamientos
\input{../tex_res/lineamientos}
