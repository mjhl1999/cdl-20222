defmodule Consultas do
  # import Operaciones

  @lista [1,2,3,4,5,6,7,8,9,10]

  def func1(str, elem, num, index) do
    import Operaciones, only: [func1: 2, func2: 3]    
    func1(str, num) |> func2(index, elem)
  end

  defmodule Consultas.Modulo do

    def func2(map, index) do
      import Operaciones, only: [func7: 1, func3: 2]
      func7(map) |> Operaciones.func3(index)
    end

    def func3(tuple, val) do
      import Operaciones, only: [func9: 2, func10: 1, func4: 1]
      func9(tuple, val) |> func10() |> func4()
    end
    
  end
end
