defmodule Operaciones do
  
  def func1(str, num) do
    import List, only: [duplicate: 2]
    duplicate(str, num)
  end

  def func2(list, index, value) do
    import List, only: [insert_at: 3]
    insert_at(list, index, value)
  end

  def func3(list, index) do
    import List, only: [delete_at: 2]
    delete_at(list, index)
  end

  def func4(list) do
    import List, only: [last: 1]
    last(list)
  end

  def func5(listss) do
    import List, only: [zip: 1]
    zip(listss)
  end

  def func6(map, key) do
    import Map, only: [delete: 2]
    delete(map, key)
  end

  def func7(map) do
    import Map, only: [to_list: 1]
    to_list(map)
  end

  def func8({x1,y1}, {x2,y2}) do
    import :math, only: [sqrt: 1, pow: 2]
    sqrt(pow((x2-x1), 2) + pow((y2-y1),2))
  end

  def func9(tuple, value) do
    import Tuple, only: [append: 2]
    append(tuple, value)
  end

  def func10(tuple) do
    import Tuple, only: [to_list: 1]
    to_list(tuple)
  end
end 
