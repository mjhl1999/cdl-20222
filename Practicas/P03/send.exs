defmodule Send do

  def readMsg do
    receive do
      msg -> IO.puts("Recibimos: #{msg}")
    end
  end

  def readMsg2 do
    receive do
      msg -> IO.puts("Recibimos :): #{msg}")
      readMsg2()
    end
  end

  def readMsg3 do
    receive do
      {:ok, msg} -> IO.puts("Recibimos :): #{msg}")
      {:nok, msg} -> IO.puts("Mmmmmm #{msg}")
      msg -> IO.puts(":::: #{msg}")
    end
    readMsg3()
  end

  def readMsgNCount(count) do
    receive do
      msg -> IO.puts("Recibimos: #{msg}, la cuenta es #{count}.")
      readMsgNCount(count + 1)
    end
  end

  def funcListen do
    receive do
      {:pid, x} -> IO.puts("Hola, #{inspect x}, soy: #{inspect(self())}.") # el proceso muere ?
      {:ok, msg} -> IO.puts("#{msg}")
      {:nok, msg} -> IO.puts("#{msg}" <> "#{msg}")
      # {:pid, x} -> IO.puts("Hola, :).")
      # {:flush, _x} -> #{inspect(flush())}
    end
    funcListen()
  end
  
end
