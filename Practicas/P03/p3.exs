defmodule Practica3 do

  def factRec(0, acc) do
    acc
  end
  def factRec(n, acc) do
    factRec(n-1, acc*n)
  end

  def prom([], acc, len) do
    div(acc, len)
  end
  def prom([head | tail], acc, len) do
    prom(tail, acc+head, len+1)
  end

  def minList([], acc) do
    acc
  end
  def minList([head | tail], acc) do
    if acc < head do
      minList(tail, acc)
    else
      minList(tail, head)
    end
  end

  def gauss(0, acc) do
    acc
  end
  def gauss(n, acc) do
    gauss(n-1, acc+n)
  end

  def imprimeMsg(_msg, 0) do
    :ok
  end
  def imprimeMsg(msg, count) do
    IO.puts(msg)
    imprimeMsg(msg, count-1)
  end

  def filterRec([], _func) do
    []
  end
  def filterRec([head | tail], func) do
    case func.(head) do
      true ->
        [head | filterRec(tail, func)]
      false ->
        filterRec(tail, func)
    end
  end

  def allRec([], _func) do
    true
  end
  def allRec([head | tail], func) do
    case func.(head) do
      false ->
        false
      true ->
        allRec(tail, func)
    end
  end

  def splitRec(list, 0, acc) do
    {acc, list}
  end
  def splitRec([], _n, _acc) do
    {[], []}
  end
  def splitRec([head | tail], n, acc) do
    splitRec(tail, n-1, acc++[head])
  end
  
end
