def schedule(arr, n):
    selected = []
    Activity.sort(key = lambda x:x[1])
    i = 0
    selected.append(arr[i])
    for j in range(i,n):
        if (arr[j][0]) >= arr[i][1]:
            selected.append(arr[j])
            i = j
    return selected

# Activity = [[5,9], [1,2], [3,4], [0,6], [5,7], [8,9]]
Activity = [[1,3], [2,5], [3,9], [6,8]]
n = len(Activity)
selected = schedule(Activity, n)
print(selected)
