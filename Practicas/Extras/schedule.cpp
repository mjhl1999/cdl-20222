#include <bits/stdc++.h>
using namespace std;

bool compareTimes(tuple<int, int> t1, tuple<int, int> t2) {
  return get<1>(t1) < get<1>(t2);
}

vector<tuple<int, int>> schedule(vector<tuple<int, int>> vectT, int n) {
  vector<tuple<int,int>> sched;
  sort(vectT.begin(), vectT.end(), compareTimes);

  int i = 0, j = 0;
  sched.push_back(vectT[i]);
  while (++j < n)
    if (get<0>(vectT[j]) >= get<1>(vectT[i])) {
      sched.push_back(vectT[j]);
      i = j;
    }

  return sched;
}

int main() {
  vector<tuple<int,int>> times {{1,3}, {2,5}, {3,9}, {8,9}};
  int size = times.size();
  vector<tuple<int, int>> sched = schedule(times, size);
  for (auto t : sched) {
    auto [s, e] = t;
    cout << "(" << s << ", " << e << ")\n";
  }
  return 0;
}
