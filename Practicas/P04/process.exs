defmodule GetBack do

  def converter do
    receive do
      {from, figuretom, data} ->
        send(from, {figuretom, data, area_formula(figuretom, data)})
        converter()
    end
  end

  defp area_formula(:circle, data) when map_size(data) == 1 do
    import :math, only: [pi: 0, pow: 2]
    pi()*pow(data[:radio],2)
    # ex 16.8 => 886.23
  end

  defp area_formula(:triangle, data) when map_size(data) == 2 do
    data[:base] * data[:height] / 2
    # ex 6x20 => 60
  end

  defp area_formula(:rectangle, data) when map_size(data) == 2 do
    data[:length] * data[:width]
    # ex 8x50 => 400
  end

  defp area_formula(:trapezoid, data) when map_size(data) == 3 do
    ((data[:base1] + data[:base2]) * data[:height]) / 2
    # ex b1:14.5, b2:20.4, h:4.1 => 71.545
  end

end
