defmodule Playground do

  def loop do
    receive do
      {:pid, pid} -> IO.puts("Hola, #{inspect pid}, soy: #{inspect(self())}.")
      loop()
    end
  end

  def spawnea(n) do
    # Enum.map(1..n, fn _ -> (spawn(Playground, :loop, [])) end)
    Enum.map(1..n,  (spawn(Playground, :loop, [])))
  end
  
  
end

# pool = Enum.map(1..100, fn _ -> (spawn(Playground, :loop, [])) end)
# Enum.each(pool, &(send(&1, {:pid, self()})))
