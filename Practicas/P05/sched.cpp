#include <bits/stdc++.h>
using namespace std;

// esto compara tuplas
bool compareTimes(tuple<int, int> t1, tuple<int, int> t2) { // funcion que recibe una tupla de enteros llamada t1 y otra llamada t2
  return get<1>(t1) < get<1>(t2); // regresa la comparación
}

vector<tuple<int, int>> schedule(vector<tuple<int, int>> vectT, int n) {
  vector<tuple<int, int>> sched;
  cout << "antes de\n";
  for (auto t : vectT) {
    auto [s, e] = t;
    cout << "(" << s << "," << e << ")\n";
  }
  sort(vectT.begin(), vectT.end(), compareTimes);
  cout << "después de\n";
  for (auto t : vectT) {
    auto [s, e] = t;
    cout << "(" << s << "," << e << ")\n";
  }
 
  int i = 0, j = 0;
  sched.push_back(vectT[i]);
  while (++j < n)
    if (get<0>(vectT[j]) >= get<1>(vectT[i])) {
      sched.push_back(vectT[j]);
      i = j;
    }
  return sched;
}

int main() {
  vector<tuple<int, int>> time {{1,3}, {2,5}, {3,9}, {6,8}};

  (1,3);
  (2,5);
  (6,8);
  (3,9);
  
  int size = time.size();
  vector<tuple<int, int>> sched = schedule(time, size);
  cout << "resultado\n";
  for (auto t : sched) {
    auto [s, e] = t;
    cout << "(" << s << "," << e << ")\n";
  }
}
