defmodule Sumitas do

  def suma(a \\ 8, b \\ :math.pi()) do
    a + b
  end
  
end
