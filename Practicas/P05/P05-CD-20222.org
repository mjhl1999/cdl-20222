#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [spanish,12pt,letterpaper]
#+LATEX_HEADER: \input{../tex_res/header.tex}
#+OPTIONS: toc:nil author:nil

#+LATEX_HEADER: \title{Computación Distribuida \\ Práctica 05}
#+LATEX_HEADER: \author{Profesor: Víctor Zamora Gutiérrez\\ Ayudante: José Ricardo Desales Santos\\ Ayudante: Diego Estrada Mejía\\ Laboratorio: Emiliano Galeana Araujo\\ Laboratorio: Miren Jessamyn Hernández Leyva}
#+LATEX_HEADER: \affil{Facultad de ciencias, UNAM}
#+DATE: Fecha de entrega: 22 de Marzo 2022

\maketitle
* Parte 1 (Conexidad)

Utilizando el archivo =graphs-print.exs= que vimos en la clase del día
$\pi$, resolver lo siguiente.

1. Crea una gráfica (disconexa) nueva  con al menos 10 vértices, añade
   un dibujo de esta (puede ser en el formato que quieras).
2. Modifica el archivo para poder  determinar si una gráfica dada es o
   no conexa. Deberás explicar qué modificaciones hiciste, por qué las
   hiciste y cómo afecta al algoritmo.

* Parte 2 (Generales)

Implementa el siguiente algoritmo en elixir.

#+BEGIN_EXAMPLE
1. alg generales(ID, input):
2.   ejecutar inicialmente
3.     if ID == Lider then
4.       send(input, 1)
5.   esperar hasta recibir un mensaje del puerto 1:
6.     M = mensaje
7.     atacar a las M horas
8.     send(ok, 1) por el puerto 1
#+END_EXAMPLE

* Hints
+ Para la parte1.
  - Utiliza  =Process.sleep(n)=  para  esperar  a que  se  ejecute  el
    intercambio de mensajes de todos los vértices y luego preguntar si
    es o no conexa.
+ Para la parte2.
  - Necesitas únicamente  una función,  esta se  encarga de  recibir y
    mandar los mensajes.
    - Necesitas dos  =receive= uno para  cuando se  es el líder  y uno
      para cuando no.
  - Los mensajes que manda el líder *SIEMPRE* son: ={self(), hora}=.
  - Los mensajes que manda quien *NO* es líder siempre son =:ok=.
  - Cuando /spawnees/ los procesos, necesitas añadir argumentos.
  - Necesitas valores por dafault
  - La entrada:
    #+BEGIN_SRC elixir
    b = spawn(Generales, :generales, [false])
    a = spawn(Generales, :generales, [true, b, 12])
    #+END_SRC

    Produce la siguiente salida:

    #+BEGIN_SRC elixir
    Recibi mensaje: Atacamos a las 12
    Comenzamos a atacar a las 12
    #+END_SRC
* Lineamientos
\input{../tex_res/lineamientos}



