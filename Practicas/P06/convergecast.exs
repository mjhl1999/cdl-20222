defmodule Convergecast do

def inicia do
  estado_inicial = %{:raiz => false, :padre => nil, :candidatos => []}
  recibe_mensaje(estado_inicial)
end

def recibe_mensaje(estado) do
  receive do
    mensaje -> {:ok, nuevo_estado} = procesa_mensaje(mensaje, estado)
    recibe_mensaje(nuevo_estado)
  end
end

def procesa_mensaje({:id, id}, estado) do
  estado = Map.put(estado, :id, id)
  {:ok, estado}
end

def procesa_mensaje({:vecinos, vecinos}, estado) do
  estado = Map.put(estado, :vecinos, vecinos)
  {:ok, estado}
end

def procesa_mensaje({:padre, padre}, estado) do
  estado = Map.put(estado, :padre, padre)
  {:ok, estado}
end

def procesa_mensaje({:respuestas}, estado) do
  estado = Map.put(estado, :respuestas, 0)
  {:ok, estado}
end

def procesa_mensaje({:raiz}, estado) do
  estado = Map.put(estado, :raiz, true)
  {:ok, estado}
end

def procesa_mensaje({:inicia, mensaje_num}, estado) do
  estado = convergecast_max(estado, mensaje_num)
  {:ok, estado}
end

def procesa_mensaje({:mensaje, mensaje}, estado) do
  %{:id => id, :respuestas => resp, :vecinos => vecinos,
    :candidatos => candidatos, :raiz => raiz} = estado
  estado = Map.put(estado, :respuestas, resp + 1)
  estado = Map.put(estado, :candidatos, candidatos ++ [mensaje])

if raiz do
  flag_l = length vecinos
  flag_r = resp + 1
  flag = flag_l == flag_r
  colorea("Soy la raíz(#{id}) y alguien me pasó un numero <#{mensaje}>\t\t", "[#{resp + 1}/#{(length vecinos)}]", flag)
else
  flag_l = (length vecinos) - 1
  flag_r = resp + 1
  flag = flag_l == flag_r
  colorea("Soy #{id} y alguien me pasó un numero <#{mensaje}>\t\t\t", "[#{resp + 1}/#{(length vecinos) - 1}]", flag)
end

estado = convergecast_max(estado, mensaje)
  {:ok, estado}
end

def colorea(text1, text2, flag) do
  if flag do
    IO.puts(IO.ANSI.cyan() <> text1 <> IO.ANSI.reset() <> IO.ANSI.green() <> text2 <> IO.ANSI.reset())
  else
    IO.puts(IO.ANSI.cyan() <> text1 <> IO.ANSI.reset() <> IO.ANSI.red() <> text2 <> IO.ANSI.reset())
  end
end

def convergecast_max(estado, mensaje_num) do
  %{:id => id, :vecinos => vecinos, :raiz => raiz,
    :respuestas => resp, :padre => daddy, :candidatos => candidatos} = estado

if raiz do
  if (resp == (length vecinos)) do
    max = Enum.reduce(candidatos, mensaje_num, fn x, acc -> max(x, acc)end)
    IO.puts(IO.ANSI.format([:blue_background, :black, inspect("<<<El elemento máximo del spanning tree es #{max}>>>")]))
    estado = Map.put(estado, :result, max)
    estado
  end
  estado

else
    if (resp == (length vecinos) - 1) do  # acabamos de recibir a los hijos
      max = Enum.reduce(candidatos, mensaje_num, fn x, acc -> max(x, acc)end)
      IO.puts("No soy raíz(#{id}) y los numeros son: #{inspect(candidatos, charlists: false)}\nY mi papá(#{inspect daddy}) recibirá #{max}")
      send(daddy, {:mensaje, max})
    else
      estado
    end
      
  end
end

end

a = spawn(Convergecast, :inicia, [])
b = spawn(Convergecast, :inicia, [])
c = spawn(Convergecast, :inicia, [])
d = spawn(Convergecast, :inicia, [])
e = spawn(Convergecast, :inicia, [])
f = spawn(Convergecast, :inicia, [])
g = spawn(Convergecast, :inicia, [])
h = spawn(Convergecast, :inicia, [])
i = spawn(Convergecast, :inicia, [])
j = spawn(Convergecast, :inicia, [])
k = spawn(Convergecast, :inicia, [])
l = spawn(Convergecast, :inicia, [])
m = spawn(Convergecast, :inicia, [])

vertices = [a,b,c,d,e,f,g,h,i,j,k,l,m]
tuples_vert = Enum.zip(1..13, vertices)

Enum.map(tuples_vert, &(send(elem(&1, 1), {:id, elem(&1, 0)})))

send(a, {:vecinos, [b,c,m]})
send(b, {:vecinos, [a,d]})
send(c, {:vecinos, [a,e,f]})
send(d, {:vecinos, [b,k,l]})
send(e, {:vecinos, [c]})
send(f, {:vecinos, [c,g,h]})
send(g, {:vecinos, [f]})
send(h, {:vecinos, [f,i,j]})
send(i, {:vecinos, [h]})
send(j, {:vecinos, [h]})
send(k, {:vecinos, [d]})
send(l, {:vecinos, [d]})
send(m, {:vecinos, [a]})

Enum.map(vertices, &(send(&1, {:respuestas})))

send(b, {:padre, a})
send(c, {:padre, a})
send(d, {:padre, b})
send(e, {:padre, c})
send(f, {:padre, c})
send(g, {:padre, f})
send(h, {:padre, f})
send(i, {:padre, h})
send(j, {:padre, h})
send(k, {:padre, d})
send(l, {:padre, d})
send(m, {:padre, a})

send(a, {:raiz})

send(k, {:inicia, 8})
send(l, {:inicia, 13})
send(e, {:inicia, 4})
send(g, {:inicia, 59})
send(i, {:inicia, 5})
send(j, {:inicia, 5})
send(m, {:inicia, 90})
