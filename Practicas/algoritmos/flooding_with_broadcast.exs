defmodule Flooding do

  def init do
    init_state = %{:root => false, :parent => nil, :children => [], :other => []}
    message_receive(init_state)
  end

  def message_receive(state) do
    receive do
      msge -> {:ok, new_state} = process_message(msge, state)
      message_receive(new_state)
    end
  end

  def process_message({:data, data}, state) do
    %{:id => id, :neighbours => ngbs} = data
    state = Map.put(state, :id, id)
    state = Map.put(state, :neighbours, ngbs)
    {:ok, state}
  end

  def process_message({:init, msg}, state) do
    state = Map.put(state, :root, true)
    %{:neighbours => ngb, :id => id} = state
    data = %{:message => msg, :from => self(), :from_id => id}
    Enum.each(ngb, &(send(&1, {:send, data})))
    {:ok, state}
  end

  def process_message({:send, data}, state) do
    %{:message => msg, :from => from, :from_id => f_id} = data
    %{:neighbours => ngbs, :id => id, :parent => parent, :root => root} = state
    new_data = %{:message => msg, :from => self(), :from_id => id}
    if parent == nil do
      if root do
        {:ok, state}
      else
        state = Map.put(state, :parent, f_id)
        send(from, {:parent, self()})
        others = ngbs -- [id]
        IO.puts("Soy #{id} (#{inspect self()}), mi papá es #{f_id}  (#{inspect from}), others: #{inspect others}")
        Enum.each(others, &(send(&1, {:send, new_data})))
        {:ok, state}
      end
    else
      send(from, {:already, self()})
      {:ok, state}
    end
  end

  def process_message({:parent, child_proc}, state) do
    %{:children => children} = state
    children = children ++ [child_proc]
    state = Map.put(state, :children, children)
    {:ok, state}
  end

  def process_message({:already, not_child}, state) do
    %{:other => other} = state
    other = other ++ [not_child]
    state = Map.put(state, :other, other)
    {:ok, state}
  end

  def process_message({:ask_child}, state) do
    %{:id => id, :children => child} = state
    IO.puts("soy #{id}, mis hijos son #{inspect(child, charlists: false)}")
    {:ok, state}
  end

  def process_message({:inicia_broadcast, mensaje}, estado) do
    estado = spanning_tree_brodcast(estado, mensaje)
    {:ok, estado}
  end

  def process_message({:mensaje, n_id, mensaje}, estado) do
    estado = spanning_tree_brodcast(estado, n_id, mensaje)
    {:ok, estado}
  end

  def spanning_tree_brodcast(estado, n_id \\ nil, message) do
    %{:id => id, :children => children, :root => root} = estado
    if root do
      Enum.map(children, fn vecino -> send vecino, {:mensaje, id, message} end)
      estado
    else
      IO.puts("soy #{id} y mi papá es #{n_id} y el mensaje es #{message}")
      Enum.map(children, fn vecino -> send vecino, {:mensaje, id, message} end)
    end
  end

end

a = spawn(Flooding, :init, [])
b = spawn(Flooding, :init, [])
c = spawn(Flooding, :init, [])
d = spawn(Flooding, :init, [])
e = spawn(Flooding, :init, [])
f = spawn(Flooding, :init, [])
g = spawn(Flooding, :init, [])
h = spawn(Flooding, :init, [])

data_a = %{:id => 1, :neighbours => [b,c]}
send(a, {:data, data_a})
data_b = %{:id => 2, :neighbours => [a,c,d,e]}
send(b, {:data, data_b})
data_c = %{:id => 3, :neighbours => [a,b,f]}
send(c, {:data, data_c})
data_d = %{:id => 4, :neighbours => [b]}
send(d, {:data, data_d})
data_e = %{:id => 5, :neighbours => [b,f,g,h]}
send(e, {:data, data_e})
data_f = %{:id => 6, :neighbours => [c,e,h]}
send(f, {:data, data_f})
data_g = %{:id => 7, :neighbours => [e,h]}
send(g, {:data, data_g})
data_h = %{:id => 8, :neighbours => [e,f,g]}
send(h, {:data, data_h})

send(a, {:init, "Hola"})
all = [a,b,c,d,e,f,g,h]
Process.sleep(1000)
Enum.each(all, &(send(&1, {:ask_child})))

Process.sleep(1000)

IO.puts(">>>>>>>>>>>")
send(a, {:inicia_broadcast, "quibo compare"})
