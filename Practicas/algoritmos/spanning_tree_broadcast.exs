defmodule Broadcast do

  def inicia do
    estado_inicial = %{:raiz => false}
    recibe_mensaje(estado_inicial)
  end

  def recibe_mensaje(estado) do
    receive do
      mensaje -> {:ok, nuevo_estado} = procesa_mensaje(mensaje, estado)
      recibe_mensaje(nuevo_estado)
    end
  end

  def procesa_mensaje({:id, id}, estado) do
    estado = Map.put(estado, :id, id)
    {:ok, estado}
  end

  def procesa_mensaje({:vecinos, vecinos}, estado) do
    estado = Map.put(estado, :vecinos, vecinos)
    {:ok, estado}
  end

  def procesa_mensaje({:raiz}, estado) do
    estado = Map.put(estado, :raiz, true)
    {:ok, estado}
  end

  def procesa_mensaje({:inicia, mensaje}, estado) do
    estado = spanning_tree_brodcast(estado, mensaje)
    {:ok, estado}
  end

  def procesa_mensaje({:mensaje, n_id, mensaje}, estado) do
    estado = spanning_tree_brodcast(estado, n_id, mensaje)
    {:ok, estado}
  end

  def spanning_tree_brodcast(estado, n_id \\ nil, mensaje) do
    %{:id => id, :vecinos => vecinos, :raiz => raiz} = estado
    if raiz do
      IO.puts(mensaje)
      Enum.map(vecinos, fn vecino -> send vecino, {:mensaje, id, mensaje} end)
      estado
    else
      IO.puts("soy #{id} y mi papá es #{n_id} y el mensaje es #{mensaje}")
      Enum.map(vecinos, fn vecino -> send vecino, {:mensaje, id, mensaje} end)
    end
  end

end

a = spawn(Broadcast, :inicia, [])
b = spawn(Broadcast, :inicia, [])
c = spawn(Broadcast, :inicia, [])
d = spawn(Broadcast, :inicia, [])
e = spawn(Broadcast, :inicia, [])
f = spawn(Broadcast, :inicia, [])
g = spawn(Broadcast, :inicia, [])

send(a, {:id, 1})
send(b, {:id, 2})
send(c, {:id, 3})
send(d, {:id, 4})
send(e, {:id, 5})
send(f, {:id, 6})
send(g, {:id, 7})

send(a, {:vecinos, [b,e]})
send(b, {:vecinos, [c,d]})
send(c, {:vecinos, []})
send(d, {:vecinos, []})
send(e, {:vecinos, [f]})
send(f, {:vecinos, []})
send(g, {:vecinos, []})

send(a, {:raiz})

send(a, {:inicia, "hola"})
