defmodule DFSFSR do

  def init do
    init_state = %{:parent => nil, :children => [], :unexplored => []}
    message_receive(init_state)
  end

  def message_receive(state) do
    receive do
      msge -> {:ok, new_state} = process_message(msge, state)
      message_receive(new_state)
    end
  end

  def process_message({:data, data}, state) do
    %{:id => id, :neighbours => ngbs} = data
    state = Map.put(state, :id, id)
    state = Map.put(state, :neighbours, ngbs)
    state = Map.put(state, :unexplored, ngbs)
    {:ok, state}
  end

  def process_message({:ask_child}, state) do
    %{:id => id, :children => child, :neighbours => ngbs, :unexplored => unexp} = state
    IO.puts("soy #{id} #{inspect self()}, mis hijos son #{inspect(child, charlists: false)}, mis vecinos son: #{inspect(ngbs, charlists: false)} y los no explorados son: #{inspect(unexp, charlists: false)}")
    {:ok, state}
  end

  def process_message({:init, msg}, state) do
    state = Map.put(state, :root, true)
    state = Map.put(state, :parent, self())
    data = %{:message => msg, :from => self()} # remove id (number)
    state = explore(state, data)
    {:ok, state}
  end

  def process_message({:send, data}, state) do
    %{:message => msg, :from => from} = data
    %{:parent => parent, :unexplored => unexp} = state
    new_data = %{:message => msg, :from => self()}
    if parent == nil do
      state = Map.put(state, :parent, from)
      nexp = List.delete(unexp, from)
      state = Map.put(state, :unexplored, nexp)
      state = explore(state, new_data)
      {:ok, state}
    else
      send(from, {:already, self(), new_data})
      state = explore(state, new_data)
      {:ok, state}
    end
  end

  def process_message({:already, _not_child, data}, state) do
    %{:message => msg} = data
    new_data = %{:message => msg, :from => self()}
    state = explore(state, new_data)
    {:ok, state}
  end
  
  def process_message({:parent, child_proc, data}, state) do
    %{:children => children} = state
    flag = Enum.member?(children, child_proc)
    if flag do
      {:ok, state}
    else
      children = children ++ [child_proc]
      state = Map.put(state, :children, children)
      state = explore(state, data)
      {:ok, state}
    end
  end

  def explore(state, data) do
    %{:unexplored => unexp, :parent => parent} = state
    %{:message => msg} = data
    new_data = %{:message => msg, :from => self()}
    if unexp != [] do
      [u | nexp] = unexp
      state = Map.put(state, :unexplored, nexp)
      send(u, {:send, new_data})
      state
    else
      if parent != self() do
        send(parent, {:parent, self(), new_data})
      end
      state
    end
  end

end

a = spawn(DFSFSR, :init, [])
b = spawn(DFSFSR, :init, [])
c = spawn(DFSFSR, :init, [])
d = spawn(DFSFSR, :init, [])
e = spawn(DFSFSR, :init, [])
f = spawn(DFSFSR, :init, [])
g = spawn(DFSFSR, :init, [])
h = spawn(DFSFSR, :init, [])

data_a = %{:id => 1, :neighbours => [b,c]}
send(a, {:data, data_a})
data_b = %{:id => 2, :neighbours => [a,c,d,e]}
send(b, {:data, data_b})
data_c = %{:id => 3, :neighbours => [a,b,f]}
send(c, {:data, data_c})
data_d = %{:id => 4, :neighbours => [b]}
send(d, {:data, data_d})
data_e = %{:id => 5, :neighbours => [b,f,g,h]}
send(e, {:data, data_e})
data_f = %{:id => 6, :neighbours => [c,e,h]}
send(f, {:data, data_f})
data_g = %{:id => 7, :neighbours => [e,h]}
send(g, {:data, data_g})
data_h = %{:id => 8, :neighbours => [e,f,g]}
send(h, {:data, data_h})

#            a/1
#          //   \
#        b/2====c/3
#       // \      \\
#       || e/5=====f/6
#       ||  ||\\     |
#       ||  || \\    |
#      d/4  ||  \\   |
#           ||   \\  |
#           g/7____h/8
send(a, {:init, "Hola"})
Process.sleep(1000)
all = [a,b,c,d,e,f,g,h]
Process.sleep(1000)
Enum.each(all, &(send(&1, {:ask_child})))
