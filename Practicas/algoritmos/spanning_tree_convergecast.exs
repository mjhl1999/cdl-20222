defmodule Convergecast do

  def inicia do
    estado_inicial = %{:raiz => false, :padre => nil, :candidatos => []}
    recibe_mensaje(estado_inicial)
  end

  def recibe_mensaje(estado) do
    receive do
      mensaje -> {:ok, nuevo_estado} = procesa_mensaje(mensaje, estado)
      recibe_mensaje(nuevo_estado)
    end
  end

  def procesa_mensaje({:id, id}, estado) do
    estado = Map.put(estado, :id, id)
    {:ok, estado}
  end

  def procesa_mensaje({:vecinos, vecinos}, estado) do
    estado = Map.put(estado, :vecinos, vecinos)
    {:ok, estado}
  end

  def procesa_mensaje({:padre, padre}, estado) do
    estado = Map.put(estado, :padre, padre)
    {:ok, estado}
  end

  def procesa_mensaje({:respuestas}, estado) do
    estado = Map.put(estado, :respuestas, 0)
    {:ok, estado}
  end

  def procesa_mensaje({:raiz}, estado) do
    estado = Map.put(estado, :raiz, true)
    {:ok, estado}
  end

  def procesa_mensaje({:inicia, mensaje_num}, estado) do
    estado = spanning_tree_convergecast_max(estado, mensaje_num)
    {:ok, estado}
  end

  def procesa_mensaje({:mensaje, mensaje}, estado) do
    %{:id => id, :respuestas => resp, :vecinos => vecinos,
      :candidatos => candidatos, :raiz => raiz} = estado
    estado = Map.put(estado, :respuestas, resp + 1)
    estado = Map.put(estado, :candidatos, candidatos ++ [mensaje])
    # IO.inspect estado
    if raiz do
      IO.puts("Soy #{id} y alguien me pasó un numero <#{mensaje}>\t\t[#{resp + 1}/#{(length vecinos)}]")
      # "Soy #{id} y alguien me pasó un numero <#{mensaje}>\t\t" <> IO.ANSI.green() <> "[#{resp + 1}/#{(length vecinos)}]" |> IO.puts
    else
      IO.puts("Soy #{id} y alguien me pasó un numero <#{mensaje}>\t\t[#{resp + 1}/#{(length vecinos) - 1}]")
    end    
    estado = spanning_tree_convergecast_max(estado, mensaje)
    {:ok, estado}
  end

  def spanning_tree_convergecast_max(estado, mensaje_num) do
    # IO.puts("#{estado}")
    %{:id => id, :vecinos => vecinos, :raiz => raiz,
      :respuestas => resp, :padre => daddy, :candidatos => candidatos} = estado
    if raiz do
      if (resp == (length vecinos)) do
        max = Enum.reduce(candidatos, mensaje_num, fn x, acc -> max(x, acc)end)
        IO.puts(IO.ANSI.format([:blue_background, :black, inspect("<<<El elemento máximo del spanning tree es #{max}>>>")]))
        # IO.puts("<<<\tEl elemento máximo del spanning tree es #{max}\t>>>")
        estado
      end
      estado
    else
      if (resp == (length vecinos) - 1) do  # acabamos de recibir a los hijos
        max = Enum.reduce(candidatos, mensaje_num, fn x, acc -> max(x, acc)end)
        IO.puts("No soy raíz(#{id}) y los numeros son: #{inspect candidatos}\nY mi papá(#{inspect daddy}) recibirá #{max}")
        send(daddy, {:mensaje, max})
        # IO.inspect estado
      else
        estado
      end
      
    end
  end

end

a = spawn(Convergecast, :inicia, [])
b = spawn(Convergecast, :inicia, [])
c = spawn(Convergecast, :inicia, [])
d = spawn(Convergecast, :inicia, [])
e = spawn(Convergecast, :inicia, [])
f = spawn(Convergecast, :inicia, [])
g = spawn(Convergecast, :inicia, [])
h = spawn(Convergecast, :inicia, [])
i = spawn(Convergecast, :inicia, [])
j = spawn(Convergecast, :inicia, [])
k = spawn(Convergecast, :inicia, [])
l = spawn(Convergecast, :inicia, [])
m = spawn(Convergecast, :inicia, [])

send(a, {:id, 1})
send(b, {:id, 2})
send(c, {:id, 3})
send(d, {:id, 4})
send(e, {:id, 5})
send(f, {:id, 6})
send(g, {:id, 7})
send(h, {:id, 8})
send(i, {:id, 9})
send(j, {:id, 10})
send(k, {:id, 11})
send(l, {:id, 12})
send(m, {:id, 13})

send(a, {:vecinos, [b,c,m]})
send(b, {:vecinos, [a,d]})
send(c, {:vecinos, [a,e,f]})
send(d, {:vecinos, [b,k,l]})
send(e, {:vecinos, [c]})
send(f, {:vecinos, [c,g,h]})
send(g, {:vecinos, [f]})
send(h, {:vecinos, [f,i,j]})
send(i, {:vecinos, [h]})
send(j, {:vecinos, [h]})
send(k, {:vecinos, [d]})
send(l, {:vecinos, [d]})
send(m, {:vecinos, [a]})

send(a, {:respuestas})
send(b, {:respuestas})
send(c, {:respuestas})
send(d, {:respuestas})
send(e, {:respuestas})
send(f, {:respuestas})
send(g, {:respuestas})
send(h, {:respuestas})
send(i, {:respuestas})
send(j, {:respuestas})
send(k, {:respuestas})
send(l, {:respuestas})
send(m, {:respuestas})

send(b, {:padre, a})
send(c, {:padre, a})
send(d, {:padre, b})
send(e, {:padre, c})
send(f, {:padre, c})
send(g, {:padre, f})
send(h, {:padre, f})
send(i, {:padre, h})
send(j, {:padre, h})
send(k, {:padre, d})
send(l, {:padre, d})
send(m, {:padre, a})



#      a/1
#      / \
#    b/2 c/3
#     |  / \
#     | e/5 f/6
#    d/4    |  \
#    / \   g/7 h/8
# k/11 l/12    / \
#            i/9 j\10




send(a, {:raiz})

send(k, {:inicia, 8})
send(l, {:inicia, 13})
send(e, {:inicia, 4})
send(g, {:inicia, 59})
send(i, {:inicia, 5})
send(j, {:inicia, 5})
send(m, {:inicia, 90})
