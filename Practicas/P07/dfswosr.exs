defmodule DFSWOSR do

  def init do
    init_state = %{:parent => nil, :leader => -1, :children => [], :unexplored => []}
    message_receive(init_state)
  end

  def message_receive(state) do
    receive do
      msge -> {:ok, new_state} = process_message(msge, state)
        # IO.puts("RECEIVE: #{inspect new_state}, MSG: #{inspect msge}")
      message_receive(new_state)
    end
  end

  def process_message({:data, data}, state) do
    %{:id => id, :neighbours => ngbs} = data
    state = Map.put(state, :id, id)
    state = Map.put(state, :neighbours, ngbs)
    state = Map.put(state, :unexplored, ngbs)
    {:ok, state}
  end

  def process_message({:ask_child}, state) do
    %{:id => id, :children => child, :neighbours => ngbs, :unexplored => unexp, :leader => leader, :parent => parent} = state
    IO.puts("soy #{id} #{inspect self()}, mis hijos son #{inspect(child, charlists: false)}, mis vecinos son: #{inspect(ngbs, charlists: false)} y los no explorados son: #{inspect(unexp, charlists: false)}. LEADER: #{leader}. PARENT: #{inspect parent}")
    {:ok, state}
  end

  def process_message({:init, msg}, state) do
    %{:id => id, :unexplored => unexp} = state
    state = Map.put(state, :leader, id)
    state = Map.put(state, :parent, self())
    data = %{:message => msg, :from => self()}
    IO.puts("Soy #{id} (#{inspect self()}). LEAD: #{id}, UNEXP: #{inspect(unexp, charlists: false)}")
    state = explore(state, data)
    {:ok, state}
  end

  def process_message({:leader, new_id, data}, state) do
    %{:message => msg, :from => from} = data
    %{:leader => leader, :neighbours => ngbs, :unexplored => _unexp} = state
    IO.puts(":::LEADER STATE(#{inspect self()} <- #{inspect from})::: #{inspect state}--- N_I: #{new_id}\n\t\t:::LEADER DATA::: #{inspect data}")
    new_data = %{:message => msg, :from => self()}
    if leader < new_id do
      state = Map.put(state, :leader, new_id)
      state = Map.put(state, :parent, from)
      state = Map.put(state, :children, [])
      nexp = List.delete(ngbs, from)
      state = Map.put(state, :unexplored, nexp)
      IO.puts(":::LEADER < NEW_ID(#{inspect self()})::: #{inspect state}")
      state = explore(state, new_data)
      {:ok, state}
    else
      IO.puts("D::::: #{inspect self()}")
      if leader == new_id do
        IO.puts(":::LEADER == NEW_ID(#{inspect self()})::: #{inspect state}")
        send(from, {:already, leader, new_data})
        {:ok, state}
      else
        {:ok, state}
      end
    end
  end

  def process_message({:already, new_id, data}, state) do
    IO.puts(":::ALREADY::: #{inspect state}---N_I: #{new_id}")
    %{:message => msg} = data
    %{:leader => leader} = state
    new_data = %{:message => msg, :from => self()}
    if new_id == leader do
      state = explore(state, new_data)
      {:ok, state}
    end
    {:ok, state}
  end

  def process_message({:parent, {child_proc, new_id}, data}, state) do
    IO.puts(":::PARENT::: #{inspect state}")
    %{:message => msg} = data
    %{:children => children, :leader => leader} = state
    new_data = %{:message => msg, :from => self()}
    if new_id == leader do
      flag = Enum.member?(children, child_proc)
      if flag do
        {:ok, state}
      else
        children = children ++ [child_proc]
        state = Map.put(state, :children, children)
        state = explore(state, new_data)
        {:ok, state}
      end
    end
  end

  def explore(state, data) do
    IO.puts(":::EXPLORE(#{inspect self()})::: #{inspect state}")
    # IO.puts("Mhe")
    %{:id => id, :unexplored => unexp, :parent => parent, :leader => leader} = state
    %{:message => msg} = data
    new_data = %{:message => msg, :from => self()}
    if unexp != [] do
      [u | nexp] = unexp
      state = Map.put(state, :unexplored, nexp)
      IO.puts(">>>>>>>>> (#{inspect self()}) #{inspect state}, U: #{inspect u}")
      send(u, {:leader, leader, new_data})
      state
    else
      # IO.puts("ADIOS?")
      if parent != self() do
        # IO.puts("HOLA?")
        send(parent, {:parent, {self(), leader}, new_data})
      end
      IO.puts("<<<<<<<<< #{inspect state}")
      state
    end
  end

end

a = spawn(DFSWOSR, :init, [])
b = spawn(DFSWOSR, :init, [])
c = spawn(DFSWOSR, :init, [])
d = spawn(DFSWOSR, :init, [])

data_a = %{:id => 1, :neighbours => [b,c]}
send(a, {:data, data_a})
data_b = %{:id => 2, :neighbours => [a,c,d]}
send(b, {:data, data_b})
data_c = %{:id => 3, :neighbours => [a,b]}
send(c, {:data, data_c})
data_d = %{:id => 4, :neighbours => [b]}
send(d, {:data, data_d})

all = [a,b,c,d]
Process.sleep(1000)
Enum.each(all, &(send(&1, {:init, "Hola"})))
Process.sleep(1000)
Enum.each(all, &(send(&1, {:ask_child})))
