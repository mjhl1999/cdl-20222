defmodule Flooding do

  def init do
    init_state = %{:root => false, :parent => nil, :children => [], :other => []}
    message_receive(init_state)
  end

  def message_receive(state) do
    receive do
      msge -> {:ok, new_state} = process_message(msge, state)
      message_receive(new_state)
    end
  end

  def process_message({:data, data}, state) do
    %{:id => id, :neighbours => ngbs} = data
    state = Map.put(state, :id, id)
    state = Map.put(state, :neighbours, ngbs)
    {:ok, state}
  end

  def process_message({:init, msg}, state) do
    state = Map.put(state, :root, true)
    %{:neighbours => ngb, :id => id} = state
    data = %{:message => msg, :from => self(), :from_id => id}
    Enum.each(ngb, &(send(&1, {:send, data})))
    {:ok, state}
  end

  def process_message({:send, data}, state) do
    %{:message => msg, :from => from, :from_id => f_id} = data
    %{:neighbours => ngbs, :id => id, :parent => parent, :root => root} = state
    new_data = %{:message => msg, :from => self(), :from_id => id}
    if parent == nil do
      if root do
        {:ok, state}
      else
        state = Map.put(state, :parent, f_id)
        send(from, {:parent, id})
        others = ngbs -- [id]
        IO.puts("Soy #{id} y me pasaron el mensaje \" #{msg} \" mi papá es #{f_id}, others: #{inspect others}")
        Enum.each(others, &(send(&1, {:send, new_data})))
        {:ok, state}
      end
    else
      send(from, {:already, self()})
      {:ok, state}
    end
  end

  def process_message({:parent, child_id}, state) do
    %{:children => children} = state
    children = children ++ [child_id]
    state = Map.put(state, :children, children)
    {:ok, state}
  end

  def process_message({:already, not_child}, state) do
    %{:other => other} = state
    other = other ++ [not_child]
    state = Map.put(state, :other, other)
    {:ok, state}
  end

  def process_message({:ask_child}, state) do
    %{:id => id, :children => child} = state
    IO.puts("soy #{id}, mis hijos son #{inspect(child, charlists: false)}")
    {:ok, state}
  end

end

a = spawn(Flooding, :init, [])
b = spawn(Flooding, :init, [])
c = spawn(Flooding, :init, [])
d = spawn(Flooding, :init, [])
e = spawn(Flooding, :init, [])
f = spawn(Flooding, :init, [])
g = spawn(Flooding, :init, [])
h = spawn(Flooding, :init, [])

data_a = %{:id => 1, :neighbours => [b,c]}
send(a, {:data, data_a})
data_b = %{:id => 2, :neighbours => [a,c,d,e]}
send(b, {:data, data_b})
data_c = %{:id => 3, :neighbours => [a,b,f]}
send(c, {:data, data_c})
data_d = %{:id => 4, :neighbours => [b]}
send(d, {:data, data_d})
data_e = %{:id => 5, :neighbours => [b,f,g,h]}
send(e, {:data, data_e})
data_f = %{:id => 6, :neighbours => [c,e,h]}
send(f, {:data, data_f})
data_g = %{:id => 7, :neighbours => [e,h]}
send(g, {:data, data_g})
data_h = %{:id => 8, :neighbours => [e,f,g]}
send(h, {:data, data_h})

send(a, {:init, "Hola"})
all = [a,b,c,d,e,f,g,h]
Process.sleep(1000)
Enum.each(all, &(send(&1, {:ask_child})))
